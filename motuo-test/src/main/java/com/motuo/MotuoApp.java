package com.motuo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: lifeng
 * @date: 2021/3/11 17:11
 * @description:
 */

@SpringBootApplication
public class MotuoApp {
	public static void main(String[] args) {


		SpringApplication.run(MotuoApp.class, args);

	}

}
