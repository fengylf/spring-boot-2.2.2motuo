package com.motuo.config;

import com.motuo.model.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: lifeng
 * @date: 2021/3/20 11:46
 * @description:
 */
@Configuration
public class Config {

	@Bean
	public Dog dog(){
		System.out.println("创建dog");
		return new Dog();
	}

}
