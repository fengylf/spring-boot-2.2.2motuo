package com.motuo.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

/**
 * @author: lifeng
 * @date: 2021/4/13 20:20
 * @description:
 */
@Component
public class MyBDRPP implements BeanDefinitionRegistryPostProcessor, PriorityOrdered {
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		BeanDefinition definition = BeanDefinitionBuilder
				.genericBeanDefinition(MyBDRPPSub.class, MyBDRPPSub::new)
				.getBeanDefinition();
		registry.registerBeanDefinition("mysubbfpp", definition);
		System.out.println("注册sub,bfpp");
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("执行postProcessBeanFactory");

	}

	@Override
	public int getOrder() {
		return 0;
	}
}
