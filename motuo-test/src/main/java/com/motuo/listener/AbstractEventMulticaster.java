package com.motuo.listener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: lifeng
 * @date: 2021/4/12 14:03
 * @description:
 */
public abstract class AbstractEventMulticaster {
	private final List<MyApplicationListener> listeners =new ArrayList<>();

	public void addListener(MyApplicationListener listener){
		listeners.add(listener);

	};

	public void removeListener(MyApplicationListener listener){
		listeners.remove(listener);
	};

	public void multicastEvent(MyApplicationEvent event) {
		doStart();
		for (MyApplicationListener listener : listeners) {
			listener.onMyApplication(event);
		}
		doEnd();
	}

	protected abstract void doStart();
	protected abstract void doEnd();
}
