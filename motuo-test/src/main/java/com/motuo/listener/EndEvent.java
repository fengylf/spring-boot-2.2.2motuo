package com.motuo.listener;

/**
 * @author: lifeng
 * @date: 2021/4/12 13:57
 * @description:
 */
public class EndEvent implements MyApplicationEvent{
	@Override
	public String getEvent() {
		return "结束事件";
	}
}
