package com.motuo.listener;

/**
 * @author: lifeng
 * @date: 2021/4/12 13:58
 * @description:
 */
public class EndListener implements MyApplicationListener{
	@Override
	public void onMyApplication(MyApplicationEvent event) {
		if (event instanceof EndEvent) {
			System.out.println("监听到结束事件");
		}
	}
}
