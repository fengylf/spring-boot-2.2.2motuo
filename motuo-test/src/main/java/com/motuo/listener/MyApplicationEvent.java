package com.motuo.listener;

/**
 * @author: lifeng
 * @date: 2021/4/12 13:50
 * @description:
 */
public interface MyApplicationEvent {
	String getEvent();
}
