package com.motuo.listener;

/**
 * @author: lifeng
 * @date: 2021/4/12 14:11
 * @description:
 */
public class MyMulticaster extends AbstractEventMulticaster{
	@Override
	protected void doStart() {
		System.out.println("开始广播");
	}

	@Override
	protected void doEnd() {
		System.out.println("结束广播");
	}
}
