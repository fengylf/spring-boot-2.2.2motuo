package com.motuo.listener;

/**
 * @author: lifeng
 * @date: 2021/4/12 13:58
 * @description:
 */
public class StartListener implements MyApplicationListener{
	@Override
	public void onMyApplication(MyApplicationEvent event) {
		if (event instanceof StartEvent) {
			System.out.println("监听到开始事件");
		}
	}
}
