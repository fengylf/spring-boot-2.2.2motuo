package com.motuo.pri;

/**
 * @author: lifeng
 * @date: 2021/3/11 18:22
 * @description:
 */
public interface Father {

	public void test(String s);
}
