package com.motuo.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author: lifeng
 * @date: 2021/3/21 0:40
 * @description:
 */
//@Configuration
public class A_OuterConfig {

	static int outer = out("outer");

	A_OuterConfig() {
		System.out.println("A_OuterConfig init...");
	}
	@Bean
	String a_o_bean(){
		System.out.println("A_OuterConfig a_o_bean init...");
		return new String();
	}

	public static int out(String s) {
		System.out.println("输出" + s);
		return 1;
	}


	@Configuration
	@Import(AInnerConfig.class)
	public static class PInnerConfig {

		static int pinner = out("pinner");
		PInnerConfig() {
			System.out.println("A_OuterConfig PInnerConfig init...");
		}
		@Bean
		String a_p_bean(){
			System.out.println("A_OuterConfig a_p_bean init...");
			return new String();
		}
	}

	//@Configuration
	private static class AInnerConfig {
		static int aInner = out("aInner");
		AInnerConfig() {
			System.out.println("A_OuterConfig InnerConfig init...");
		}
		@Bean
		String a_i_bean(){
			System.out.println("A_OuterConfig a_i_bean init...");
			return new String();
		}
	}
}
